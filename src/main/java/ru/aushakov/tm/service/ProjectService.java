package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public Project add(final String name, final String description) {
        if (StringUtils.isEmpty(name)) return null;
        if (StringUtils.isEmpty(description)) return null;
        final Project project = new Project(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (StringUtils.isEmpty(id)) return null;
        if (StringUtils.isEmpty(name)) return null;
        return projectRepository.updateOneById(id, name, description);
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        if (!NumberUtil.isValidIndex(index)) return null;
        if (StringUtils.isEmpty(name)) return null;
        return projectRepository.updateOneByIndex(index, name, description);
    }

    @Override
    public Project startOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return projectRepository.startOneById(id);
    }

    @Override
    public Project startOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return projectRepository.startOneByIndex(index);
    }

    @Override
    public Project startOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return projectRepository.startOneByName(name);
    }

    @Override
    public Project finishOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return projectRepository.finishOneById(id);
    }

    @Override
    public Project finishOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return projectRepository.finishOneByIndex(index);
    }

    @Override
    public Project finishOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return projectRepository.finishOneByName(name);
    }

    @Override
    public Project changeOneStatusById(final String id, final String statusId) {
        if (StringUtils.isEmpty(id)) return null;
        if (StringUtils.isEmpty(statusId)) return null;
        return projectRepository.changeOneStatusById(id, Status.valueOf(statusId));
    }

    @Override
    public Project changeOneStatusByIndex(final Integer index, final String statusId) {
        if (!NumberUtil.isValidIndex(index)) return null;
        if (StringUtils.isEmpty(statusId)) return null;
        return projectRepository.changeOneStatusByIndex(index, Status.valueOf(statusId));
    }

    @Override
    public Project changeOneStatusByName(final String name, final String statusId) {
        if (StringUtils.isEmpty(name)) return null;
        if (StringUtils.isEmpty(statusId)) return null;
        return projectRepository.changeOneStatusByName(name, Status.valueOf(statusId));
    }

}
