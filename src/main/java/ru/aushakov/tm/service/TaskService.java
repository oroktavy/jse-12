package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.ITaskService;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public Task add(final String name, final String description) {
        if (StringUtils.isEmpty(name)) return null;
        if (StringUtils.isEmpty(description)) return null;
        final Task task = new Task(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task updateOneById(final String id, final String name, final String description) {
        if (StringUtils.isEmpty(id)) return null;
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.updateOneById(id, name, description);
    }

    @Override
    public Task updateOneByIndex(final Integer index, final String name, final String description) {
        if (!NumberUtil.isValidIndex(index)) return null;
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.updateOneByIndex(index, name, description);
    }

    @Override
    public Task startOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return taskRepository.startOneById(id);
    }

    @Override
    public Task startOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return taskRepository.startOneByIndex(index);
    }

    @Override
    public Task startOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.startOneByName(name);
    }

    @Override
    public Task finishOneById(final String id) {
        if (StringUtils.isEmpty(id)) return null;
        return taskRepository.finishOneById(id);
    }

    @Override
    public Task finishOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) return null;
        return taskRepository.finishOneByIndex(index);
    }

    @Override
    public Task finishOneByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.finishOneByName(name);
    }

    @Override
    public Task changeOneStatusById(final String id, final String statusId) {
        if (StringUtils.isEmpty(id)) return null;
        if (StringUtils.isEmpty(statusId)) return null;
        return taskRepository.changeOneStatusById(id, Status.valueOf(statusId));
    }

    @Override
    public Task changeOneStatusByIndex(final Integer index, final String statusId) {
        if (!NumberUtil.isValidIndex(index)) return null;
        if (StringUtils.isEmpty(statusId)) return null;
        return taskRepository.changeOneStatusByIndex(index, Status.valueOf(statusId));
    }

    @Override
    public Task changeOneStatusByName(final String name, final String statusId) {
        if (StringUtils.isEmpty(name)) return null;
        if (StringUtils.isEmpty(statusId)) return null;
        return taskRepository.changeOneStatusByName(name, Status.valueOf(statusId));
    }

}
