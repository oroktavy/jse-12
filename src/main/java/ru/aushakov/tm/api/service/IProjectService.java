package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    void add(Project project);

    Project add(String name, String description);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

    Project startOneById(String id);

    Project startOneByIndex(Integer index);

    Project startOneByName(String name);

    Project finishOneById(String id);

    Project finishOneByIndex(Integer index);

    Project finishOneByName(String name);

    Project changeOneStatusById(String id, String statusId);

    Project changeOneStatusByIndex(Integer index, String statusId);

    Project changeOneStatusByName(String name, String statusId);

}
