package ru.aushakov.tm.api.controller;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showOneById();

    void showOneByIndex();

    void showOneByName();

    void removeOneById();

    void removeOneByIndex();

    void removeOneByName();

    void updateOneById();

    void updateOneByIndex();

    void startOneById();

    void startOneByIndex();

    void startOneByName();

    void finishOneById();

    void finishOneByIndex();

    void finishOneByName();

    void changeOneStatusById();

    void changeOneStatusByIndex();

    void changeOneStatusByName();

}
