package ru.aushakov.tm.model;

import org.apache.commons.lang3.StringUtils;

public class Command {

    private String name = "";

    private String arg = "";

    private String description = "";

    public Command() {
    }

    public Command(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (!StringUtils.isEmpty(this.name)) result += this.name;
        if (!StringUtils.isEmpty(this.arg)) result += ": [" + this.arg + "]";
        if (!StringUtils.isEmpty(this.description)) result += " - " + this.description;
        return result;
    }
}
